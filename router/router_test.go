package router

import (
	"testing"

	user_handlers "gitlab.com/alex.ognezvezd/mygolangservice/modules/user/handlers"
	citation_handlers "gitlab.com/alex.ognezvezd/mygolangservice/modules/citation/handlers"
)

func TestNewRouter(t *testing.T) {
	uh := user_handlers.NewUserHandler(nil, nil)
	NewRouter(citation_handlers.NewQuotesHandler(nil, uh.Login), uh)
}