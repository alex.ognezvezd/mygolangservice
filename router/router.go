// Package router implements a router
package router

import (
	"github.com/go-chi/chi/v5"

	citation_handler "gitlab.com/alex.ognezvezd/mygolangservice/modules/citation/handlers"
	user_handler "gitlab.com/alex.ognezvezd/mygolangservice/modules/user/handlers"
)

// NewRouter creates a new router
func NewRouter(qh *citation_handler.QuotesHandler, uh *user_handler.UserHandler) *chi.Mux {
	router := chi.NewRouter()

	router.Post("/quote/create", qh.CreateQuote)
	router.Get("/quote/get/{id}", qh.GetQuote)
	router.Put("/quote/update/{id}", qh.UpdateQuote)
	router.Delete("/quote/delete/{id}", qh.DeleteQuote)
	router.Get("/quote/random", qh.GetRandom)
	router.Get("/authentication", uh.Authentication)

	return router
}
