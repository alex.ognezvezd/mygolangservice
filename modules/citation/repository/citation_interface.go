package repository

import (
	"context"

	"gitlab.com/alex.ognezvezd/mygolangservice/modules/citation/entity"
	"gitlab.com/golight/dao/params"
)

type Quoteser interface {
	Create(ctx context.Context, dto entity.Quote) (int64, error)
	Update(ctx context.Context, dto entity.Quote) error
	GetByID(ctx context.Context, quotesID int) (entity.Quote, error)
	GetList(ctx context.Context, condition params.Condition) ([]entity.Quote, error)
	Delete(ctx context.Context, quotesID int) error
	GetRandom() (*entity.Quote, error)
}
