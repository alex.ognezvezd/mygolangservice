// Package repository imlements handling with dao
package repository

import (
	"context"
	"errors"
	"fmt"
	rand "math/rand"

	"gitlab.com/alex.ognezvezd/mygolangservice/modules/citation/entity"
	"gitlab.com/golight/dao"
	"gitlab.com/golight/dao/params"
	"gitlab.com/golight/dao/types"

	"time"
)

// QuotesStorage is a struct to handle with dao
type QuotesStorage struct {
	adapter dao.DAOFace // object to handle with dao
}

// NewQuotesStorage is a constructor of a QuotesStorage
func NewQuotesStorage(sqlAdapter dao.DAOFace) (QuotesStorage, error) {
	if sqlAdapter == nil {
		return QuotesStorage{}, errors.New("adapter is nil")
	}
	return QuotesStorage{adapter: sqlAdapter}, nil
}

// Create creates a new row in the table
func (q *QuotesStorage) Create(ctx context.Context, dto entity.Quote) (int64, error) {
	return q.adapter.Create(ctx, &dto)
}

// Update updates a row in the table
func (q *QuotesStorage) Update(ctx context.Context, dto entity.Quote) error {
	return q.adapter.Update(
		ctx,
		&dto,
		params.Condition{
			Equal: map[string]interface{}{"id": dto.ID},
		},
		params.Update,
	)
}

// GetByID returns a row with the current index
func (q *QuotesStorage) GetByID(ctx context.Context, quotesID int) (entity.Quote, error) {
	var list []entity.Quote
	var table entity.Quote
	err := q.adapter.List(ctx, &list, &table, params.Condition{
			Equal: map[string]interface{}{"id": quotesID},
		})

	if err != nil {
		return entity.Quote{}, err
	}
	if len(list) < 1 {
		return entity.Quote{}, fmt.Errorf("quotes storage: GetByID not found")
	}
	return list[0], err
}

// GetList ruturns rows of the table by condition
func (q *QuotesStorage) GetList(ctx context.Context, condition params.Condition) ([]entity.Quote, error) {
	var list []entity.Quote
	var table entity.Quote
	err := q.adapter.List(ctx, &list, &table, condition)
	if err != nil {
		return nil, err
	}

	return list, nil
}

// Delete deletes a row in the table by current index
func (q *QuotesStorage) Delete(ctx context.Context, quotesID int) error {
	table, err := q.GetByID(ctx, quotesID)
	if err != nil {
		return err
	}

	table.DeletedAt = types.NewNullTime(time.Now())

	return q.adapter.Update(
		ctx,
		&table,
		params.Condition{
			Equal: map[string]interface{}{"id": table.ID},
		},
		params.Update,
	)
}

// GetRandom requests dao List Method and returns random row
func (q *QuotesStorage) GetRandom() (*entity.Quote, error) {
	res := []entity.Quote{}
	err := q.adapter.List(context.Background(), &res, &entity.Quote{}, params.Condition{})
	if err != nil {
		return nil, err
	}
	if len(res) == 0 {
		return nil, errors.New("there are No Data")
	}
	randDataIdx := rand.Intn(len(res))
	for i, quote := range res {
		if i == randDataIdx && quote.ID > -1 {
			return &quote, nil
		}
	}
	return nil, errors.New("there are No Data")
}
