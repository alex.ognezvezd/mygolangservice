package cache

import (
	"errors"
	"testing"

	"github.com/golang/mock/gomock"
	"gitlab.com/alex.ognezvezd/mygolangservice/modules/citation/entity"
	qmock "gitlab.com/alex.ognezvezd/mygolangservice/modules/citation/repository/mocks"
)

func TestCreate(t *testing.T) {
	type args struct {
		dto entity.Quote
	}
	tests := []struct {
		name            string
		args            args
		prepareCacher   func(cacher *qmock.MockCacher)
		prepareQuoteser func(quoteser *qmock.MockQuoteser)
		wantErr         bool
	}{
		{
			name: "StatusCode",
			args: args{
				dto: entity.Quote{},
			},
			prepareCacher: func(cacher *qmock.MockCacher) {
				cacher.EXPECT().Set(gomock.Any(), "0", &entity.Quote{}, cacheTTL).
					Times(1).
					Return(nil)
			},
			prepareQuoteser: func(quoteser *qmock.MockQuoteser) {
				quoteser.EXPECT().Create(gomock.Any(), gomock.AssignableToTypeOf(entity.Quote{})).
					Times(1).
					Return(int64(0), nil)
			},
			wantErr: false,
		},
		{
			name: "Error q.storage.Create()",
			args: args{
				dto: entity.Quote{},
			},
			prepareCacher: func(cacher *qmock.MockCacher) {
				cacher.EXPECT().Set(gomock.Any(), "0", &entity.Quote{}, cacheTTL).
					Times(0).
					Return(nil)
			},
			prepareQuoteser: func(quoteser *qmock.MockQuoteser) {
				quoteser.EXPECT().Create(gomock.Any(), gomock.AssignableToTypeOf(entity.Quote{})).
					Times(1).
					Return(int64(0), errors.New("some error"))
			},
			wantErr: true,
		},
		{
			name: "Error q.cache.Set()",
			args: args{
				dto: entity.Quote{},
			},
			prepareCacher: func(cacher *qmock.MockCacher) {
				cacher.EXPECT().Set(gomock.Any(), "0", &entity.Quote{}, cacheTTL).
					Times(1).
					Return(errors.New("some error"))
			},
			prepareQuoteser: func(quoteser *qmock.MockQuoteser) {
				quoteser.EXPECT().Create(gomock.Any(), gomock.AssignableToTypeOf(entity.Quote{})).
					Times(1).
					Return(int64(0), nil)
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			quoteser := qmock.NewMockQuoteser(gomock.NewController(t))
			cacher := qmock.NewMockCacher(gomock.NewController(t))

			tt.prepareCacher(cacher)
			tt.prepareQuoteser(quoteser)

			quotesCache, err := NewQuotesCache(quoteser, cacher)
			if err != nil {
				t.Error(err)
			}

			_, err = quotesCache.Create(&tt.args.dto)
			if (err != nil) != tt.wantErr {
				t.Errorf("Create() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}

}

func TestList(t *testing.T) {
	type args struct {
		id int
	}
	tests := []struct {
		name             string
		args             args
		prepareCacherGet func(cacher *qmock.MockCacher)
		prepareCacherSet func(cacher *qmock.MockCacher)
		prepareQuoteser  func(quoteser *qmock.MockQuoteser)
		want             *entity.Quote
		wantErr          bool
	}{
		{
			name: "StatusCode",
			args: args{
				id: 1,
			},
			prepareCacherGet: func(cacher *qmock.MockCacher) {
				var quote entity.Quote
				cacher.EXPECT().Get(gomock.Any(), "1", quote).
					Times(1).
					Return(errors.New("cache miss"))
			},
			prepareCacherSet: func(cacher *qmock.MockCacher) {
				cacher.EXPECT().Set(gomock.Any(), "1", &entity.Quote{}, cacheTTL).
					Times(1).
					Return(nil)
			},
			prepareQuoteser: func(quoteser *qmock.MockQuoteser) {
				quoteser.EXPECT().GetByID(gomock.Any(), 1).
					Times(1).
					Return(entity.Quote{}, nil)
			},
			want:    &entity.Quote{},
			wantErr: false,
		},
		{
			name: "From Cache",
			args: args{
				id: 1,
			},
			prepareCacherGet: func(cacher *qmock.MockCacher) {
				var quote entity.Quote
				cacher.EXPECT().Get(gomock.Any(), "1", quote).
					Times(1).
					Return(nil)
			},
			prepareCacherSet: func(cacher *qmock.MockCacher) {
				cacher.EXPECT().Set(gomock.Any(), "1", &entity.Quote{}, cacheTTL).
					Times(0).
					Return(nil)
			},
			prepareQuoteser: func(quoteser *qmock.MockQuoteser) {
				quoteser.EXPECT().GetByID(gomock.Any(), 1).
					Times(0).
					Return(entity.Quote{}, nil)
			},
			want:    &entity.Quote{},
			wantErr: false,
		},
		{
			name: "Error q.cache.Set()",
			args: args{
				id: 1,
			},
			prepareCacherGet: func(cacher *qmock.MockCacher) {
				var quote entity.Quote
				cacher.EXPECT().Get(gomock.Any(), "1", quote).
					Times(1).
					Return(errors.New("cache miss"))
			},
			prepareCacherSet: func(cacher *qmock.MockCacher) {
				cacher.EXPECT().Set(gomock.Any(), "1", &entity.Quote{}, cacheTTL).
					Times(1).
					Return(errors.New("some error"))
			},
			prepareQuoteser: func(quoteser *qmock.MockQuoteser) {
				quoteser.EXPECT().GetByID(gomock.Any(), 1).
					Times(1).
					Return(entity.Quote{}, nil)
			},
			want:    &entity.Quote{},
			wantErr: true,
		},
		{
			name: "Error q.storage.GetByID()",
			args: args{
				id: 1,
			},
			prepareCacherGet: func(cacher *qmock.MockCacher) {
				var quote entity.Quote
				cacher.EXPECT().Get(gomock.Any(), "1", quote).
					Times(1).
					Return(errors.New("cache miss"))
			},
			prepareCacherSet: func(cacher *qmock.MockCacher) {
				cacher.EXPECT().Set(gomock.Any(), "1", &entity.Quote{}, cacheTTL).
					Times(0).
					Return(nil)
			},
			prepareQuoteser: func(quoteser *qmock.MockQuoteser) {
				quoteser.EXPECT().GetByID(gomock.Any(), 1).
					Times(1).
					Return(entity.Quote{}, errors.New("some error"))
			},
			want:    &entity.Quote{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			quoteser := qmock.NewMockQuoteser(gomock.NewController(t))
			cacher := qmock.NewMockCacher(gomock.NewController(t))

			tt.prepareCacherGet(cacher)
			tt.prepareCacherSet(cacher)
			tt.prepareQuoteser(quoteser)

			quotesCache, err := NewQuotesCache(quoteser, cacher)
			if err != nil {
				t.Error(err)
			}

			_, err = quotesCache.List(tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("List() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}

func TestGetRandom(t *testing.T) {
	tests := []struct {
		name            string
		prepareQuoteser func(quoteser *qmock.MockQuoteser)
		want            *entity.Quote
		wantErr         bool
	}{
		{
			name: "StatusCode",
			prepareQuoteser: func(quoteser *qmock.MockQuoteser) {
				quoteser.EXPECT().GetRandom().
					Times(1).
					Return(&entity.Quote{}, nil)
			},
			want:    &entity.Quote{},
			wantErr: false,
		},
		{
			name: "Error q.storage.GetRandom()",
			prepareQuoteser: func(quoteser *qmock.MockQuoteser) {
				quoteser.EXPECT().GetRandom().
					Times(1).
					Return(&entity.Quote{}, errors.New("some error"))
			},
			want:    &entity.Quote{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			quoteser := qmock.NewMockQuoteser(gomock.NewController(t))
			cacher := qmock.NewMockCacher(gomock.NewController(t))

			tt.prepareQuoteser(quoteser)

			quotesCache, err := NewQuotesCache(quoteser, cacher)
			if err != nil {
				t.Error(err)
			}

			_, err = quotesCache.GetRandom()
			if (err != nil) != tt.wantErr {
				t.Errorf("GetRandom() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}

func TestUpdate(t *testing.T) {
	type args struct {
		dto entity.Quote
		id  int
	}
	tests := []struct {
		name                string
		args                args
		prepareCacherSet    func(cacher *qmock.MockCacher)
		prepareCacherDelete func(cacher *qmock.MockCacher)
		prepareQuoteser     func(quoteser *qmock.MockQuoteser)
		wantErr             bool
	}{
		{
			name: "StatusCode",
			args: args{
				dto: entity.Quote{},
				id: 1,
			},
			prepareCacherSet: func(cacher *qmock.MockCacher) {
				cacher.EXPECT().Set(gomock.Any(), "1", &entity.Quote{}, cacheTTL).
					Times(1).
					Return(nil)
			},
			prepareCacherDelete: func(cacher *qmock.MockCacher) {
				cacher.EXPECT().Delete(gomock.Any(), false, "1").
					Times(1).
					Return(nil)
			},
			prepareQuoteser: func(quoteser *qmock.MockQuoteser) {
				quoteser.EXPECT().Update(gomock.Any(), gomock.AssignableToTypeOf(entity.Quote{})).
					Times(1).
					Return(nil)
			},
			wantErr: false,
		},
		{
			name: "Error q.cache.Set()",
			args: args{
				dto: entity.Quote{},
				id: 1,
			},
			prepareCacherSet: func(cacher *qmock.MockCacher) {
				cacher.EXPECT().Set(gomock.Any(), "1", &entity.Quote{}, cacheTTL).
					Times(1).
					Return(errors.New("some error"))
			},
			prepareCacherDelete: func(cacher *qmock.MockCacher) {
				cacher.EXPECT().Delete(gomock.Any(), false, "1").
					Times(1).
					Return(nil)
			},
			prepareQuoteser: func(quoteser *qmock.MockQuoteser) {
				quoteser.EXPECT().Update(gomock.Any(), gomock.AssignableToTypeOf(entity.Quote{})).
					Times(1).
					Return(nil)
			},
			wantErr: true,
		},
		{
			name: "Error q.cache.Delete()",
			args: args{
				dto: entity.Quote{},
				id: 1,
			},
			prepareCacherSet: func(cacher *qmock.MockCacher) {
				cacher.EXPECT().Set(gomock.Any(), "1", &entity.Quote{}, cacheTTL).
					Times(0).
					Return(nil)
			},
			prepareCacherDelete: func(cacher *qmock.MockCacher) {
				cacher.EXPECT().Delete(gomock.Any(), false, "1").
					Times(1).
					Return(errors.New("some error"))
			},
			prepareQuoteser: func(quoteser *qmock.MockQuoteser) {
				quoteser.EXPECT().Update(gomock.Any(), gomock.AssignableToTypeOf(entity.Quote{})).
					Times(1).
					Return(nil)
			},
			wantErr: true,
		},
		{
			name: "Error q.storage.Update()",
			args: args{
				dto: entity.Quote{},
				id: 1,
			},
			prepareCacherSet: func(cacher *qmock.MockCacher) {
				cacher.EXPECT().Set(gomock.Any(), "1", &entity.Quote{}, cacheTTL).
					Times(0).
					Return(nil)
			},
			prepareCacherDelete: func(cacher *qmock.MockCacher) {
				cacher.EXPECT().Delete(gomock.Any(), false, "1").
					Times(0).
					Return(nil)
			},
			prepareQuoteser: func(quoteser *qmock.MockQuoteser) {
				quoteser.EXPECT().Update(gomock.Any(), gomock.AssignableToTypeOf(entity.Quote{})).
					Times(1).
					Return(errors.New("some error"))
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			quoteser := qmock.NewMockQuoteser(gomock.NewController(t))
			cacher := qmock.NewMockCacher(gomock.NewController(t))

			tt.prepareCacherSet(cacher)
			tt.prepareCacherDelete(cacher)
			tt.prepareQuoteser(quoteser)

			quotesCache, err := NewQuotesCache(quoteser, cacher)
			if err != nil {
				t.Error(err)
			}

			err = quotesCache.Update(&tt.args.dto, tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("Update() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}

func TestDelete(t *testing.T) {
	type args struct {
		dto entity.Quote
		id  int
	}
	tests := []struct {
		name                string
		args                args
		prepareCacherSet    func(cacher *qmock.MockCacher)
		prepareCacherDelete func(cacher *qmock.MockCacher)
		prepareQuoteser     func(quoteser *qmock.MockQuoteser)
		wantErr             bool
	}{
		{
			name: "StatusCode",
			args: args{
				dto: entity.Quote{},
				id: 1,
			},
			prepareCacherSet: func(cacher *qmock.MockCacher) {
				cacher.EXPECT().Set(gomock.Any(), "1", &entity.Quote{}, cacheTTL).
					Times(1).
					Return(nil)
			},
			prepareCacherDelete: func(cacher *qmock.MockCacher) {
				cacher.EXPECT().Delete(gomock.Any(), false, "1").
					Times(1).
					Return(nil)
			},
			prepareQuoteser: func(quoteser *qmock.MockQuoteser) {
				quoteser.EXPECT().Delete(gomock.Any(), 1).
					Times(1).
					Return(nil)
			},
			wantErr: false,
		},
		{
			name: "Error q.cache.Set()",
			args: args{
				dto: entity.Quote{},
				id: 1,
			},
			prepareCacherSet: func(cacher *qmock.MockCacher) {
				cacher.EXPECT().Set(gomock.Any(), "1", &entity.Quote{}, cacheTTL).
					Times(1).
					Return(errors.New("some error"))
			},
			prepareCacherDelete: func(cacher *qmock.MockCacher) {
				cacher.EXPECT().Delete(gomock.Any(), false, "1").
					Times(1).
					Return(nil)
			},
			prepareQuoteser: func(quoteser *qmock.MockQuoteser) {
				quoteser.EXPECT().Delete(gomock.Any(), 1).
					Times(1).
					Return(nil)
			},
			wantErr: true,
		},
		{
			name: "Error q.cache.Delete()",
			args: args{
				dto: entity.Quote{},
				id: 1,
			},
			prepareCacherSet: func(cacher *qmock.MockCacher) {
				cacher.EXPECT().Set(gomock.Any(), "1", &entity.Quote{}, cacheTTL).
					Times(0).
					Return(nil)
			},
			prepareCacherDelete: func(cacher *qmock.MockCacher) {
				cacher.EXPECT().Delete(gomock.Any(), false, "1").
					Times(1).
					Return(errors.New("some error"))
			},
			prepareQuoteser: func(quoteser *qmock.MockQuoteser) {
				quoteser.EXPECT().Delete(gomock.Any(), 1).
					Times(1).
					Return(nil)
			},
			wantErr: true,
		},
		{
			name: "Error q.storage.Delete()",
			args: args{
				dto: entity.Quote{},
				id: 1,
			},
			prepareCacherSet: func(cacher *qmock.MockCacher) {
				cacher.EXPECT().Set(gomock.Any(), "1", &entity.Quote{}, cacheTTL).
					Times(0).
					Return(nil)
			},
			prepareCacherDelete: func(cacher *qmock.MockCacher) {
				cacher.EXPECT().Delete(gomock.Any(), false, "1").
					Times(0).
					Return(nil)
			},
			prepareQuoteser: func(quoteser *qmock.MockQuoteser) {
				quoteser.EXPECT().Delete(gomock.Any(), 1).
					Times(1).
					Return(errors.New("some error"))
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			quoteser := qmock.NewMockQuoteser(gomock.NewController(t))
			cacher := qmock.NewMockCacher(gomock.NewController(t))

			tt.prepareCacherSet(cacher)
			tt.prepareCacherDelete(cacher)
			tt.prepareQuoteser(quoteser)

			quotesCache, err := NewQuotesCache(quoteser, cacher)
			if err != nil {
				t.Error(err)
			}

			err = quotesCache.Delete(&tt.args.dto, tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("Delete() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}
