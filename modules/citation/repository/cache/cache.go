// Package cache uses cache to speed up work with database
package cache

import (
	"context"
	"errors"
	"strconv"
	"time"

	"gitlab.com/alex.ognezvezd/mygolangservice/modules/citation/entity"
	repository "gitlab.com/alex.ognezvezd/mygolangservice/modules/citation/repository"
	"gitlab.com/golight/cache"
)

// cacheTTL is a time that cache lives
const (
	cacheTTL = 60 * time.Minute
)

// QuotesCache implements a cache object
type QuotesCache struct {
	storage repository.Quoteser // object handling with dao
	cache   cache.Cacher              // redis cache
}

// NewQuotesCache is a constructor of QuotesCache
func NewQuotesCache(s repository.Quoteser, c cache.Cacher) (*QuotesCache, error) {
	if s == nil || c == nil {
		return nil, errors.New("invalid memory address or nil pointer dereference")
	}
	return &QuotesCache{storage: s, cache: c}, nil
}

// Create creates a new data base row and sets it in cache
func (qc *QuotesCache) Create(ent *entity.Quote) (int64, error) {
	idx, err := qc.storage.Create(context.Background(), *ent)
	if err != nil {
		return -1, err
	}
	return idx, qc.cache.Set(context.Background(), strconv.Itoa(int(idx)), ent, cacheTTL)
}

// List returns a data base row by id and sets it in cache
func (qc *QuotesCache) List(id int) (*entity.Quote, error) {
	var quote entity.Quote
	err := qc.cache.Get(context.Background(), strconv.Itoa(id), quote)
	if err == nil {
		return &quote, nil
	}

	quote, err = qc.storage.GetByID(context.Background(), id)
	if err != nil {
		return nil, err
	}
	return &quote, qc.cache.Set(context.Background(), strconv.Itoa(id), &quote, cacheTTL)
}

// GetRandom returns a random quote from a data base
func (qc *QuotesCache) GetRandom() (*entity.Quote, error) {
	return qc.storage.GetRandom()
}

// Update updates a quote with current id in cache and updates the quote in a data base
func (qc *QuotesCache) Update(ent *entity.Quote, idx int) error {
	err := qc.storage.Update(context.Background(), *ent)
	if err != nil {
		return err
	}

	err = qc.cache.Delete(context.Background(), false, strconv.Itoa(idx))
	if err != nil {
		return err
	}
	return qc.cache.Set(context.Background(), strconv.Itoa(idx), ent, cacheTTL)
}

// Delete deletes a quote with current id in cache and deletes the quote in a data base
func (qc *QuotesCache) Delete(ent *entity.Quote, idx int) error {
	err := qc.storage.Delete(context.Background(), idx)
	if err != nil {
		return err
	}

	err = qc.cache.Delete(context.Background(), false, strconv.Itoa(idx))
	if err != nil {
		return err
	}
	return qc.cache.Set(context.Background(), strconv.Itoa(idx), ent, cacheTTL)
}
