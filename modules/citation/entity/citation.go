// Package entity implements entities of the project
package entity

import (
	"time"

	"gitlab.com/golight/dao/types"
)

// QuoteDTO структура для передачи данных цитаты без служебной информации
type QuoteDTO struct {
	ID     int
	Text   string
	Author string
}

// Quote структура для хранения цитаты в базе данных
// тэги для автоматической миграции и валидации
// ID - идентификатор цитаты
// Text - текст цитаты
// Author - автор цитаты
// CreatedAt - дата создания цитаты
// UpdatedAt - дата обновления цитаты
// DeletedAt - дата удаления цитаты
//
//go:generate genstorage -filename=$GOFILE
type Quote struct {
	ID        int            `json:"-" db:"id" db_type:"BIGSERIAL primary key" db_default:"not null" db_ops:"id"`
	Text      string         `json:"text" db:"text" db_ops:"create,conflict,upsert" db_type:"varchar(512)" db_default:"not null" db_index:"index,unique"`
	Author    string         `json:"author" db:"author" db_ops:"create,update,upsert" db_type:"varchar(144)" db_default:"not null"`
	CreatedAt time.Time      `json:"-" db:"created_at" db_type:"timestamp" db_default:"default (now()) not null" db_index:"index"`
	UpdatedAt time.Time      `json:"-" db:"updated_at" db_ops:"update" db_type:"timestamp" db_default:"default (now()) not null" db_index:"index"`
	DeletedAt types.NullTime `json:"-" db:"deleted_at" db_ops:"update" db_type:"timestamp" db_default:"default null" db_index:"index"`
}

// TableName возвращает имя таблицы, авто сгенерированный метод
func (*Quote) TableName() string {
	return "quotes"
}

// OnCreate возвращает список полей для автоматической валидации, авто сгенерированный метод
func (q *Quote) OnCreate() []string {
	return []string{}
}

// FieldsPointers возвращает список указателей на поля структуры, авто сгенерированный метод (для предотвращения лишних аллокаций, в процессе рефлексии)
func (q *Quote) FieldsPointers() []interface{} {
	return []interface{}{
		&q.ID,
		&q.Text,
		&q.Author,
		&q.CreatedAt,
		&q.UpdatedAt,
		&q.DeletedAt,
	}
}
