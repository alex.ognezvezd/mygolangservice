// Package handlers handles http requests
package handlers

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"

	"gitlab.com/alex.ognezvezd/mygolangservice/modules/citation/repository/cache"
	"gitlab.com/alex.ognezvezd/mygolangservice/modules/citation/entity"
)

// QuotesHandler allows to handle requests
type QuotesHandler struct {
	cache       *cache.QuotesCache // cache object
	authenticationFunc func(w http.ResponseWriter, r *http.Request) bool
}

// NewQuotesHandler is a constructor of QuotesHandler
func NewQuotesHandler(c *cache.QuotesCache, authFunc func(w http.ResponseWriter, r *http.Request) bool) *QuotesHandler {
	return &QuotesHandler{
		cache:       c,
		authenticationFunc: authFunc,
	}
}

// CreateQuote allows to create a new quote
func (qh *QuotesHandler) CreateQuote(w http.ResponseWriter, r *http.Request) {
	if !qh.authenticationFunc(w, r) {
		return
	}

	ent := &entity.Quote{}
	var idx int64
	err := json.NewDecoder(r.Body).Decode(&ent)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	idx, err = qh.cache.Create(ent)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		//Postgres doesn't support LastInsertId. Data was inserted
		return
	}
	log.Printf("Created at %d index\n", idx)
}

// GetQuote displays a new quote by id
func (qh *QuotesHandler) GetQuote(w http.ResponseWriter, r *http.Request) {
	if !qh.authenticationFunc(w, r) {
		return
	}

	id, err := getIdFromURL(r.URL.Path)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	quote, err := qh.cache.List(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	_, err = w.Write([]byte(fmt.Sprintf("%#v", quote)))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
	log.Printf("%#v", quote)
}

// GetRandom displays a random quote
func (qh *QuotesHandler) GetRandom(w http.ResponseWriter, r *http.Request) {
	if !qh.authenticationFunc(w, r) {
		return
	}

	quote, err := qh.cache.GetRandom()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	_, err = w.Write([]byte(quote.Text))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
	log.Println(quote.Text)
}

// UpdateQuote updates a quote by id
func (qh *QuotesHandler) UpdateQuote(w http.ResponseWriter, r *http.Request) {
	if !qh.authenticationFunc(w, r) {
		return
	}

	idx, err := getIdFromURL(r.URL.Path)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	ent := &entity.Quote{}
	err = json.NewDecoder(r.Body).Decode(&ent)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	err = qh.cache.Update(ent, idx)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	_, err = w.Write([]byte(fmt.Sprintf("Updated at %d index", idx)))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
	log.Printf("Updated at %d index", idx)
}

// DeleteQuote deletes a quote by id
func (qh *QuotesHandler) DeleteQuote(w http.ResponseWriter, r *http.Request) {
	if !qh.authenticationFunc(w, r) {
		return
	}

	idx, err := getIdFromURL(r.URL.Path)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	ent := &entity.Quote{}
	err = json.NewDecoder(r.Body).Decode(&ent)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	err = qh.cache.Delete(ent, idx)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	_, err = w.Write([]byte(fmt.Sprintf("Deleted at %d index", idx)))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
	log.Printf("Deleted at %d index", idx)
}

// getIdFromURL returns id from url
func getIdFromURL(url string) (int, error) {
	rawURL := strings.Split(url, "/")
	idRaw := rawURL[len(rawURL)-1]
	return strconv.Atoi(idRaw)
}
