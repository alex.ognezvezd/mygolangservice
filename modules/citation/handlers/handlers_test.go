package handlers

import (
	"errors"
	"net/http"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	"github.com/steinfletcher/apitest"
	citation_entity "gitlab.com/alex.ognezvezd/mygolangservice/modules/citation/entity"
	"gitlab.com/alex.ognezvezd/mygolangservice/modules/citation/repository/cache"
	qmock "gitlab.com/alex.ognezvezd/mygolangservice/modules/citation/repository/mocks"
	"gitlab.com/alex.ognezvezd/mygolangservice/modules/user/entity"
	"gitlab.com/alex.ognezvezd/mygolangservice/modules/user/handlers"
	umock "gitlab.com/alex.ognezvezd/mygolangservice/modules/user/repository/mocks"
	"gitlab.com/golight/dao/params"
)

func TestCreateQuote(t *testing.T) {
	quoteser := qmock.NewMockQuoteser(gomock.NewController(t))
	userser := umock.NewMockUserser(gomock.NewController(t))
	cacher := qmock.NewMockCacher(gomock.NewController(t))

	quoteser.EXPECT().Create(gomock.Any(), gomock.AssignableToTypeOf(citation_entity.Quote{})).Times(1).Return(int64(0), nil)
	userser.EXPECT().GetList(gomock.Any(), params.Condition{}).Times(1).Return([]entity.User{}, nil)
	userser.EXPECT().Create(gomock.Any(), gomock.AssignableToTypeOf(entity.User{})).Times(1).Return(int64(0), nil)
	cacher.EXPECT().Set(gomock.Any(), "0", gomock.AssignableToTypeOf(&citation_entity.Quote{}), gomock.Any()).Times(1).Return(nil)

	quote := &citation_entity.Quote{}
	quote.OnCreate()
	quote.FieldsPointers()
	quote.TableName()
	authProperties := &entity.User{
		Username: "WildChild",
		Password: "bodomAfterMidnight2000",
	}
	uh := handlers.NewUserHandler(authProperties, userser)
	c, err := cache.NewQuotesCache(quoteser, cacher)
	if err != nil {
		t.Error(err)
	}
	quotesHandler := NewQuotesHandler(c, uh.Login)
	token := authorise(t, uh)

	apitest.New(). // configuration
			HandlerFunc(quotesHandler.CreateQuote).
			Post("/quote/create"). // request
			JSON(`{"Author": "Meow", "Text": "Meow-meow-meow. Meow!"}`).
			Cookie(token.Name, token.Value).
			Expect(t).       // expectations
			Status(http.StatusOK).
			End()
	
	quoteser.EXPECT().Create(gomock.Any(), gomock.AssignableToTypeOf(citation_entity.Quote{})).Times(1).Return(int64(0), errors.New("some error"))
	cacher.EXPECT().Set(gomock.Any(), "0", gomock.AssignableToTypeOf(&citation_entity.Quote{}), gomock.Any()).Times(0).Return(nil)

	apitest.New(). // configuration
			HandlerFunc(quotesHandler.CreateQuote).
			Post("/quote/create"). // request
			JSON(`{"Author": "Meow", "Text": "Meow-meow-meow. Meow!"}`).
			Cookie(token.Name, token.Value).
			Expect(t).       // expectations
			Status(http.StatusInternalServerError).
			End()

	apitest.New(). // configuration
			HandlerFunc(quotesHandler.CreateQuote).
			Post("/quote/create"). // request
			Cookie(token.Name, token.Value).
			Expect(t).       // expectations
			Status(http.StatusInternalServerError).
			End()
}

func TestGetQuote(t *testing.T) {
	quoteser := qmock.NewMockQuoteser(gomock.NewController(t))
	userser := umock.NewMockUserser(gomock.NewController(t))
	cacher := qmock.NewMockCacher(gomock.NewController(t))

	userser.EXPECT().GetList(gomock.Any(), params.Condition{}).Times(1).Return([]entity.User{}, nil)
	userser.EXPECT().Create(gomock.Any(), gomock.AssignableToTypeOf(entity.User{})).Times(1).Return(int64(0), nil)
	cacher.EXPECT().Get(gomock.Any(), "1", gomock.AssignableToTypeOf(citation_entity.Quote{})).Times(1).Return(nil)

	authProperties := &entity.User{
		Username: "WildChild",
		Password: "bodomAfterMidnight2000",
	}
	uh := handlers.NewUserHandler(authProperties, userser)
	c, err := cache.NewQuotesCache(quoteser, cacher)
	if err != nil {
		t.Error(err)
	}
	quotesHandler := NewQuotesHandler(c, uh.Login)

	apitest.New(). // configuration
			HandlerFunc(quotesHandler.GetQuote).
			Get("/quote/get/1"). // request
			Expect(t).       // expectations
			Status(http.StatusUnauthorized).
			End()

	token := authorise(t, uh)

	apitest.New(). // configuration
			HandlerFunc(quotesHandler.GetQuote).
			Get("/quote/get/1"). // request
			Cookie(token.Name, token.Value).
			Expect(t).       // expectations
			Status(http.StatusOK).
			End()

	apitest.New(). // configuration
			HandlerFunc(quotesHandler.GetQuote).
			Get("/quote/get/meow"). // request
			Cookie(token.Name, token.Value).
			Expect(t).       // expectations
			Status(http.StatusInternalServerError).
			End()

	cacher.EXPECT().Get(gomock.Any(), "1", gomock.AssignableToTypeOf(citation_entity.Quote{})).Times(1).Return(errors.New("cache is empty"))
	quoteser.EXPECT().GetByID(gomock.Any(), 1).Times(1).Return(citation_entity.Quote{}, errors.New("some error"))

	apitest.New(). // configuration
			HandlerFunc(quotesHandler.GetQuote).
			Get("/quote/get/1"). // request
			Cookie(token.Name, token.Value).
			Expect(t).       // expectations
			Status(http.StatusInternalServerError).
			End()
}

func TestGetRandom(t *testing.T) {
	quoteser := qmock.NewMockQuoteser(gomock.NewController(t))
	userser := umock.NewMockUserser(gomock.NewController(t))
	cacher := qmock.NewMockCacher(gomock.NewController(t))

	userser.EXPECT().GetList(gomock.Any(), params.Condition{}).Times(1).Return([]entity.User{}, nil)
	userser.EXPECT().Create(gomock.Any(), gomock.AssignableToTypeOf(entity.User{})).Times(1).Return(int64(0), nil)

	authProperties := &entity.User{
		Username: "WildChild",
		Password: "bodomAfterMidnight2000",
	}
	uh := handlers.NewUserHandler(authProperties, userser)
	c, err := cache.NewQuotesCache(quoteser, cacher)
	if err != nil {
		t.Error(err)
	}
	quotesHandler := NewQuotesHandler(c, uh.Login)

	apitest.New(). // configuration
			HandlerFunc(quotesHandler.GetRandom).
			Get("/quote/random"). // request
			Expect(t).       // expectations
			Status(http.StatusUnauthorized).
			End()

	token := authorise(t, uh)
	quoteser.EXPECT().GetRandom().Times(1).Return(&citation_entity.Quote{}, nil)

	apitest.New(). // configuration
			HandlerFunc(quotesHandler.GetRandom).
			Get("/quote/random"). // request
			Cookie(token.Name, token.Value).
			Expect(t).       // expectations
			Status(http.StatusOK).
			End()

	quoteser.EXPECT().GetRandom().Times(1).Return(&citation_entity.Quote{}, errors.New("some error"))

	apitest.New(). // configuration
			HandlerFunc(quotesHandler.GetRandom).
			Get("/quote/random"). // request
			Cookie(token.Name, token.Value).
			Expect(t).       // expectations
			Status(http.StatusInternalServerError).
			End()
}

func TestUpdateQuote(t *testing.T) {
	quoteser := qmock.NewMockQuoteser(gomock.NewController(t))
	userser := umock.NewMockUserser(gomock.NewController(t))
	cacher := qmock.NewMockCacher(gomock.NewController(t))

	userser.EXPECT().GetList(gomock.Any(), params.Condition{}).Times(1).Return([]entity.User{}, nil)
	userser.EXPECT().Create(gomock.Any(), gomock.AssignableToTypeOf(entity.User{})).Times(1).Return(int64(0), nil)

	authProperties := &entity.User{
		Username: "WildChild",
		Password: "bodomAfterMidnight2000",
	}
	uh := handlers.NewUserHandler(authProperties, userser)
	c, err := cache.NewQuotesCache(quoteser, cacher)
	if err != nil {
		t.Error(err)
	}
	quotesHandler := NewQuotesHandler(c, uh.Login)

	apitest.New(). // configuration
			HandlerFunc(quotesHandler.UpdateQuote).
			Put("/quote/update/1"). // request
			JSON(`{"ID": 1, "Author": "Lermontov", "UpdatedAt": "` + time.Now().GoString() + `"}`).
			Expect(t).       // expectations
			Status(http.StatusUnauthorized).
			End()

	token := authorise(t, uh)
	quoteser.EXPECT().Update(gomock.Any(), gomock.AssignableToTypeOf(citation_entity.Quote{})).Times(1).Return(nil)
	cacher.EXPECT().Delete(gomock.Any(), false, "1").Times(1).Return(nil)
	cacher.EXPECT().Set(gomock.Any(), "1", gomock.AssignableToTypeOf(&citation_entity.Quote{}), gomock.Any()).Times(1).Return(nil)

	apitest.New(). // configuration
			HandlerFunc(quotesHandler.UpdateQuote).
			Put("/quote/update/1"). // request
			JSON(`{"ID": 1, "Author": "Lermontov", "UpdatedAt": "` + time.Now().GoString() + `"}`).
			Cookie(token.Name, token.Value).
			Expect(t).       // expectations
			Status(http.StatusOK).
			End()

	apitest.New(). // configuration
			HandlerFunc(quotesHandler.UpdateQuote).
			Put("/quote/update/1"). // request
			Cookie(token.Name, token.Value).
			Expect(t).       // expectations
			Status(http.StatusInternalServerError).
			End()

	apitest.New(). // configuration
			HandlerFunc(quotesHandler.UpdateQuote).
			Put("/quote/update/meow"). // request
			JSON(`{"ID": 1, "Author": "Lermontov", "UpdatedAt": "` + time.Now().GoString() + `"}`).
			Cookie(token.Name, token.Value).
			Expect(t).       // expectations
			Status(http.StatusInternalServerError).
			End()

	quoteser.EXPECT().Update(gomock.Any(), gomock.AssignableToTypeOf(citation_entity.Quote{})).Times(1).Return(errors.New("some error"))

	apitest.New(). // configuration
			HandlerFunc(quotesHandler.UpdateQuote).
			Put("/quote/update/1"). // request
			JSON(`{"ID": 1, "Author": "Lermontov", "UpdatedAt": "` + time.Now().GoString() + `"}`).
			Cookie(token.Name, token.Value).
			Expect(t).       // expectations
			Status(http.StatusInternalServerError).
			End()
}

func TestDeleteQuote(t *testing.T) {
	quoteser := qmock.NewMockQuoteser(gomock.NewController(t))
	userser := umock.NewMockUserser(gomock.NewController(t))
	cacher := qmock.NewMockCacher(gomock.NewController(t))

	userser.EXPECT().GetList(gomock.Any(), params.Condition{}).Times(1).Return([]entity.User{}, nil)
	userser.EXPECT().Create(gomock.Any(), gomock.AssignableToTypeOf(entity.User{})).Times(1).Return(int64(0), nil)

	authProperties := &entity.User{
		Username: "WildChild",
		Password: "bodomAfterMidnight2000",
	}
	uh := handlers.NewUserHandler(authProperties, userser)
	c, err := cache.NewQuotesCache(quoteser, cacher)
	if err != nil {
		t.Error(err)
	}
	quotesHandler := NewQuotesHandler(c, uh.Login)

	apitest.New(). // configuration
			HandlerFunc(quotesHandler.DeleteQuote).
			Delete("/quote/delete/1"). // request
			JSON(`{"ID": 1, "Author": "DELETED", "DeletedAt": "` + time.Now().GoString() + `"}`).
			Expect(t).       // expectations
			Status(http.StatusUnauthorized).
			End()

	token := authorise(t, uh)
	quoteser.EXPECT().Delete(gomock.Any(), 1).Times(1).Return(nil)
	cacher.EXPECT().Delete(gomock.Any(), false, "1").Times(1).Return(nil)
	cacher.EXPECT().Set(gomock.Any(), "1", gomock.AssignableToTypeOf(&citation_entity.Quote{}), gomock.Any()).Times(1).Return(nil)

	apitest.New(). // configuration
			HandlerFunc(quotesHandler.DeleteQuote).
			Delete("/quote/delete/1"). // request
			JSON(`{"ID": 1, "Author": "DELETED", "DeletedAt": "` + time.Now().GoString() + `"}`).
			Cookie(token.Name, token.Value).
			Expect(t).       // expectations
			Status(http.StatusOK).
			End()

	apitest.New(). // configuration
			HandlerFunc(quotesHandler.DeleteQuote).
			Delete("/quote/delete/1"). // request
			Cookie(token.Name, token.Value).
			Expect(t).       // expectations
			Status(http.StatusInternalServerError).
			End()

	apitest.New(). // configuration
			HandlerFunc(quotesHandler.DeleteQuote).
			Delete("/quote/delete/meow"). // request
			JSON(`{"ID": 1, "Author": "DELETED", "DeletedAt": "` + time.Now().GoString() + `"}`).
			Cookie(token.Name, token.Value).
			Expect(t).       // expectations
			Status(http.StatusInternalServerError).
			End()

	quoteser.EXPECT().Delete(gomock.Any(), 1).Times(1).Return(errors.New("some error"))

	apitest.New(). // configuration
			HandlerFunc(quotesHandler.DeleteQuote).
			Delete("/quote/delete/1"). // request
			JSON(`{"ID": 1, "Author": "DELETED", "DeletedAt": "` + time.Now().GoString() + `"}`).
			Cookie(token.Name, token.Value).
			Expect(t).       // expectations
			Status(http.StatusInternalServerError).
			End()
}

func TestErrors(t *testing.T) {
	quoteser := qmock.NewMockQuoteser(gomock.NewController(t))
	userser := umock.NewMockUserser(gomock.NewController(t))
	cacher := qmock.NewMockCacher(gomock.NewController(t))

	authProperties := &entity.User{
		Username: "WildChild",
		Password: "bodomAfterMidnight2000",
	}
	uh := handlers.NewUserHandler(authProperties, userser)
	c, err := cache.NewQuotesCache(quoteser, cacher)
	if err != nil {
		t.Error(err)
	}
	quotesHandler := NewQuotesHandler(c, uh.Login)

	userser.EXPECT().GetList(gomock.Any(), params.Condition{}).Times(2).Return([]entity.User{}, nil)
	userser.EXPECT().Create(gomock.Any(), gomock.AssignableToTypeOf(entity.User{})).Times(1).Return(int64(0), nil)

	apitest.New(). // configuration
			HandlerFunc(quotesHandler.CreateQuote).
			Post("/quote/create"). // request
			Expect(t).       // expectations
			Status(http.StatusUnauthorized).
			End()

	authorise(t, uh)
	
	apitest.New(). // configuration
			HandlerFunc(quotesHandler.CreateQuote).
			Post("/quote/create"). // request
			Cookie("token", "token.Value").
			Expect(t).       // expectations
			Status(http.StatusUnauthorized).
			End()

	apitest.New(). // configuration
			HandlerFunc(uh.Authentication).
			Get("/authentication"). // request
			BasicAuth("WildChild", "bodomAfterMidnight2001").
			Expect(t).       // expectations
			Status(http.StatusUnauthorized).
			End()
}

func authorise(t *testing.T, userHandler *handlers.UserHandler) *http.Cookie {
	res := apitest.New(). // configuration
			HandlerFunc(userHandler.Authentication).
			Get("/authentication"). // request
			BasicAuth("WildChild", "bodomAfterMidnight2000").
			Expect(t).       // expectations
			Status(http.StatusOK).
			CookiePresent("token").
			End()
	return res.Response.Cookies()[0]
}