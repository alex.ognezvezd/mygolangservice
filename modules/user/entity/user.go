// Package entity implements entities of the project
package entity

import (
	"time"

	"gitlab.com/golight/dao/types"
)

// User is a struct for storing authorized users
// tags for automatical migration and validation
// ID - index of user
// Username - name of user
// Password - password of user
// CreatedAt - date of creating a user
// UpdatedAt - date of updating a user
// DeletedAt - date of deleting a user
//
//go:generate genstorage -filename=$GOFILE
type User struct {
	ID        int            `json:"-" db:"id" db_type:"BIGSERIAL primary key" db_default:"not null" db_ops:"id"`
	Username  string         `json:"username" db:"username" db_ops:"create,conflict,upsert" db_type:"varchar(512)" db_default:"not null" db_index:"index,unique"`
	Password  string         `json:"password" db:"password" db_ops:"create,update,upsert" db_type:"varchar(144)" db_default:"not null"`
	CreatedAt time.Time      `json:"-" db:"created_at" db_type:"timestamp" db_default:"default (now()) not null" db_index:"index"`
	UpdatedAt time.Time      `json:"-" db:"updated_at" db_ops:"update" db_type:"timestamp" db_default:"default (now()) not null" db_index:"index"`
	DeletedAt types.NullTime `json:"-" db:"deleted_at" db_ops:"update" db_type:"timestamp" db_default:"default null" db_index:"index"`
}

func (u *User) TableName() string {
	return "users"
}

func (u *User) OnCreate() []string {
	return []string{}
}

func (u *User) FieldsPointers() []interface{} {
	return []interface{}{
		&u.ID,
		&u.Username,
		&u.Password,
		&u.CreatedAt,
		&u.UpdatedAt,
		&u.DeletedAt,
	}
}
