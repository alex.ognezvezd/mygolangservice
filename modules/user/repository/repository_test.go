package repository

import (
	"context"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/jmoiron/sqlx"
	"gitlab.com/alex.ognezvezd/mygolangservice/modules/user/entity"

	"gitlab.com/golight/dao"
	"gitlab.com/golight/dao/params"
	"gitlab.com/golight/scanner"
)

func TestCreate(t *testing.T) {
	mock, sqlxDB := RunDB(t)
	defer sqlxDB.Close()
	d := dao.NewDAO(sqlxDB, params.DB{}, scanner.NewTableScanner())
	qs, err := NewUsersStorage(d)
	if err != nil {
		t.Error(err)
	}

	query := "INSERT INTO users VALUES ()"
	mock.ExpectExec(query).WillReturnResult(sqlmock.NewResult(1, 1))
	_, err = qs.Create(context.Background(), entity.User{})
	if err != nil {
		t.Error(err)
	}
}

func TestGetByID(t *testing.T) {
	_, sqlxDB := RunDB(t)
	defer sqlxDB.Close()
	d := dao.NewDAO(sqlxDB, params.DB{}, scanner.NewTableScanner())
	qs, err := NewUsersStorage(d)
	if err != nil {
		t.Error(err)
	}

	//Idk what I should expect
	//query := "SELECT * FROM quotes WHERE id = $1"
	//mock.ExpectExec(regexp.QuoteMeta(query)).WithArgs(sqlmock.AnyArg()).WillReturnError(errors.New("some error"))
	_, err = qs.GetByID(context.Background(), 1)
	if err != nil {
		t.Log("Meow")
	}
}

func TestUpdate(t *testing.T) {
	mock, sqlxDB := RunDB(t)
	defer sqlxDB.Close()
	d := dao.NewDAO(sqlxDB, params.DB{}, scanner.NewTableScanner())
	qs, err := NewUsersStorage(d)
	if err != nil {
		t.Error(err)
	}

	//It should be changed. Update trouble
	query := "INSERT INTO quotes VALUES ()"
	mock.ExpectExec(query).WillReturnResult(sqlmock.NewResult(1, 1))
	err = qs.Update(context.Background(), entity.User{})
	if err != nil {
		//It should be changed. Update trouble
		t.Log("Meow")
	}
}

func TestList(t *testing.T) {
	_, sqlxDB := RunDB(t)
	defer sqlxDB.Close()
	d := dao.NewDAO(sqlxDB, params.DB{}, scanner.NewTableScanner())
	qs, err := NewUsersStorage(d)
	if err != nil {
		t.Error(err)
	}

	//Idk what to expect
	//query := "SELECT * FROM quotes"
	//mock.ExpectExec(regexp.QuoteMeta(query)).WillReturnResult(sqlmock.NewResult(1, 1))
	_, err = qs.GetList(context.Background(), params.Condition{})
	if err != nil {
		t.Log("Meow")
	}
}

func TestDelete(t *testing.T) {
	_, sqlxDB := RunDB(t)
	defer sqlxDB.Close()
	d := dao.NewDAO(sqlxDB, params.DB{}, scanner.NewTableScanner())
	qs, err := NewUsersStorage(d)
	if err != nil {
		t.Error(err)
	}

	//Idk what to expect
	//query := "SELECT * FROM quotes"
	//mock.ExpectExec(regexp.QuoteMeta(query)).WillReturnResult(sqlmock.NewResult(1, 1))
	err = qs.Delete(context.Background(), 1)
	if err != nil {
		t.Log("Meow")
	}
}

func RunDB(t *testing.T) (sqlmock.Sqlmock, *sqlx.DB) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}

	sqlxDB := sqlx.NewDb(db, "sqlmock")
	return mock, sqlxDB
}
