package repository

import (
	"context"

	"gitlab.com/alex.ognezvezd/mygolangservice/modules/user/entity"
	"gitlab.com/golight/dao/params"
)

type Userser interface {
	Create(ctx context.Context, dto entity.User) (int64, error)
	Update(ctx context.Context, dto entity.User) error
	GetByID(ctx context.Context, usersID int) (entity.User, error)
	GetList(ctx context.Context, condition params.Condition) ([]entity.User, error)
	Delete(ctx context.Context, usersID int) error
}