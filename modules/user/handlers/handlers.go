// Package handlers handles http requests
package handlers

import (
	"context"
	"crypto/sha256"
	"crypto/subtle"
	"net/http"
	"time"

	"gitlab.com/alex.ognezvezd/mygolangservice/modules/user/entity"
	"gitlab.com/alex.ognezvezd/mygolangservice/modules/user/repository"
	"gitlab.com/alex.ognezvezd/mygolangservice/modules/user/service"
	"gitlab.com/golight/dao/params"
)

// UserHandler allows to handle requests
type UserHandler struct {
	authProperties *entity.User       // authentication properties like a username and a password
	userRepository repository.Userser // users repository
}

// NewUserHandler is a constructor of UserHandler
func NewUserHandler(authProp *entity.User, uRep repository.Userser) *UserHandler {
	return &UserHandler{
		authProperties: authProp,
		userRepository: uRep,
	}
}

// Login is a function for logging in
func (*UserHandler) Login(w http.ResponseWriter, r *http.Request) bool {
	c, err := r.Cookie("token")
	if err != nil {
		if err == http.ErrNoCookie {
			// If the cookie is not set, return an unauthorized status
			http.Error(w, "There's no authentication token", http.StatusUnauthorized)
			return false
		}
		// For any other type of error, return a bad request status
		http.Error(w, "Error getting authentication token", http.StatusUnauthorized)
		return false
	}
	ok, err := service.VerifyToken(c.Value)
	if err != nil {
		http.Error(w, "Invalid authentication token", http.StatusUnauthorized)
		return false
	}
	return ok
}

// Authentication checks if the user has a valid JWT token
func (uh *UserHandler) Authentication(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("WWW-Authenticate", `Basic realm="mydomain"`)
	username, password, ok := r.BasicAuth()
	if ok {
		isCorrectUser, err := uh.checkDBUser(username, password)
		if err != nil {
			http.Error(w, "Error requesting to the database", http.StatusOK)
		}

		if !isCorrectUser {
			usernameHash := sha256.Sum256([]byte(username))
			passwordHash := sha256.Sum256([]byte(password))
			expectedUsernameHash := sha256.Sum256([]byte(uh.authProperties.Username))
			expectedPasswordHash := sha256.Sum256([]byte(uh.authProperties.Password))

			usernameMatch := (subtle.ConstantTimeCompare(usernameHash[:], expectedUsernameHash[:]) == 1)
			passwordMatch := (subtle.ConstantTimeCompare(passwordHash[:], expectedPasswordHash[:]) == 1)

			if usernameMatch && passwordMatch {
				isCorrectUser = true
				err := uh.createNewUser(username, password)
				if err != nil {
					_, err := w.Write([]byte("Error creating user in a database"))
					if err != nil {
						http.Error(w, err.Error(), http.StatusInternalServerError)
					}
				}
			}
		}

		if isCorrectUser {
			token, err := service.GenerateToken(username)
			if err != nil {
				http.Error(w, "Error generating token", http.StatusInternalServerError)
			}

			http.SetCookie(w, &http.Cookie{
				Name:    "token",
				Value:   token,
				Expires: time.Now().Add(time.Hour * 24),
			})

			return
		}
	}
	w.Header().Set("WWW-Authenticate", `Basic realm="restricted", charset="UTF-8"`)
	http.Error(w, "Unauthorized", http.StatusUnauthorized)
}

func (uh *UserHandler) checkDBUser(username string, password string) (bool, error) {
	var users []entity.User
	users, err := uh.userRepository.GetList(context.Background(), params.Condition{})
	if err != nil {
		return false, err
	}
	for _, user := range users {
		if user.Username == username && user.Password == password {
			return true, nil
		}
	}
	return false, nil
}

func (uh *UserHandler) createNewUser(username string, password string) error {
	_, err := uh.userRepository.Create(context.Background(), entity.User{Username: username, Password: password})
	return err
}
