# Build stage
FROM golang:1.21.7-alpine AS build
WORKDIR /go/src/gitlab.com/alex.ognezvezd/mygolangservice

COPY go.mod go.sum ./
RUN go mod download && apk add --no-cache ca-certificates

# Final stage
FROM scratch
COPY --from=build /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=build /app /app


CMD ["/app"]

