package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	jsoniter "github.com/json-iterator/go"
	"github.com/ptflp/godecoder"
	conf "gitlab.com/alex.ognezvezd/mygolangservice/config"
	mycache "gitlab.com/alex.ognezvezd/mygolangservice/modules/citation/repository/cache"
	"gitlab.com/alex.ognezvezd/mygolangservice/modules/user/entity"
	citation_entity "gitlab.com/alex.ognezvezd/mygolangservice/modules/citation/entity"
	user_handlers "gitlab.com/alex.ognezvezd/mygolangservice/modules/user/handlers"
	citation_handlers "gitlab.com/alex.ognezvezd/mygolangservice/modules/citation/handlers"
	user_repository "gitlab.com/alex.ognezvezd/mygolangservice/modules/user/repository"
	citation_repository "gitlab.com/alex.ognezvezd/mygolangservice/modules/citation/repository"
	"gitlab.com/alex.ognezvezd/mygolangservice/router"
	"gitlab.com/golight/cache"
	"gitlab.com/golight/dao"
	"gitlab.com/golight/dao/connector"
	"gitlab.com/golight/loggerx"
	"gitlab.com/golight/migrator"
	"gitlab.com/golight/scanner"
	"go.uber.org/zap"
)

func main() {

	appConf, err := conf.NewAppConfig()
	if err != nil {
		log.Fatal(err)
	}

	ts := scanner.NewTableScanner()
	ent := &citation_entity.Quote{}
	ent_user := &entity.User{}
	ts.RegisterTable(ent)
	ts.RegisterTable(ent_user)

	logger := loggerx.InitLogger(appConf.Name, appConf.Production)
	var sqlDB *connector.SqlDB
	sqlDB, err = connector.NewSqlDB(appConf.Db, ts, logger)
	if err != nil {
		logger.Fatal("failed to connect to db", zap.Error(err))
	}
	defer sqlDB.DB.Close()

	err = migrator.NewMigrator(sqlDB.DB, appConf.Db, ts).Migrate()
	if err != nil {
		logger.Fatal("failed to migrate", zap.Error(err))
	}

	daoSample := dao.NewDAO(sqlDB.DB, appConf.Db, ts)
	authProperties := &entity.User{
		Username: os.Getenv("AUTH_USERNAME"),
		Password: os.Getenv("AUTH_PASSWORD"),
	}
	if authProperties.Username == "" || authProperties.Password == "" {
		log.Fatal("basic auth properties must be provided")
	}

	serv, err := citation_repository.NewQuotesStorage(daoSample)
	if err != nil {
		log.Fatal(err)
	}
	serv_user, err := user_repository.NewUsersStorage(daoSample)
	if err != nil {
		log.Fatal(err)
	}

	decoder := godecoder.NewDecoder(jsoniter.Config{
		EscapeHTML:             true,
		SortMapKeys:            true,
		ValidateJsonRawMessage: true,
		DisallowUnknownFields:  true,
	})

	golightCache, err := cache.NewCache(appConf.Cache, decoder)
	if err != nil {
		log.Fatal(err)
	}
	c, err := mycache.NewQuotesCache(&serv, golightCache)
	if err != nil {
		log.Fatal(err)
	}
	uh := user_handlers.NewUserHandler(authProperties, &serv_user)
	qh := citation_handlers.NewQuotesHandler(c, uh.Login)
	r := router.NewRouter(qh, uh)

	server := http.Server{
		Addr:         ":4080",
		Handler:      r,
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 10 * time.Second,
	}

	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		log.Println("Starting server...")
		if err := server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("Server error: %v", err)
		}
	}()

	<-sigChan

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	err = server.Shutdown(ctx)
	if err != nil {
		log.Fatalf("Server shutdown error: %v", err)
	}

	log.Println("Server stopped gracefully")

}
