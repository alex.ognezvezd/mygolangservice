package main

import (
	"bytes"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/http/cookiejar"
	"time"
)

func main() {
	jar, err := cookiejar.New(nil)
	handleError(err)
	client := &http.Client{
		Jar: jar,
	}
	url := "http://localhost:4080/"
	log.Printf("URL:> %s\n\n", url)

	url = "http://localhost:4080/authentication"
	log.Println("URL:>", url)
	req, err := http.NewRequest("GET", url, &bytes.Buffer{})
	handleError(err)
	req.SetBasicAuth("WildChild", "bodomAfterMidnight2000")
	doRequest(req, client, err)

	url = "http://localhost:4080/quote/create"
	log.Println("URL:>", url)
	//Change text, so it will be unique
	var jsonStr = []byte(`{"Author": "Meow", "Text": "Meow-meow-meow. Meow!"}`)
	req, err = http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	handleError(err)
	doRequest(req, client, err)

	url = "http://localhost:4080/quote/update/1"
	log.Println("URL:>", url)
	jsonStr = []byte(`{"ID": 1, "Author": "Lermontov", "UpdatedAt": "` + time.Now().GoString() + `"}`)
	req, err = http.NewRequest("PUT", url, bytes.NewBuffer(jsonStr))
	handleError(err)
	doRequest(req, client, err)

	url = "http://localhost:4080/quote/get/1"
	log.Println("URL:>", url)
	jsonStr = []byte(``)
	req, err = http.NewRequest("GET", url, bytes.NewBuffer(jsonStr))
	handleError(err)
	doRequest(req, client, err)

	url = "http://localhost:4080/quote/update/1"
	log.Println("URL:>", url)
	jsonStr = []byte(`{"ID": 1, "Author": "Fallout", "UpdatedAt": "` + time.Now().GoString() + `"}`)
	req, err = http.NewRequest("PUT", url, bytes.NewBuffer(jsonStr))
	handleError(err)
	doRequest(req, client, err)

	url = "http://localhost:4080/quote/get/1"
	log.Println("URL:>", url)
	jsonStr = []byte(``)
	req, err = http.NewRequest("GET", url, bytes.NewBuffer(jsonStr))
	handleError(err)
	doRequest(req, client, err)

	url = "http://localhost:4080/quote/delete/1"
	log.Println("URL:>", url)
	jsonStr = []byte(`{"ID": 1, "Author": "DELETED", "DeletedAt": "` + time.Now().GoString() + `"}`)
	req, err = http.NewRequest("DELETE", url, bytes.NewBuffer(jsonStr))
	handleError(err)
	doRequest(req, client, err)

	url = "http://localhost:4080/quote/get/1"
	log.Println("URL:>", url)
	jsonStr = []byte(``)
	req, err = http.NewRequest("GET", url, bytes.NewBuffer(jsonStr))
	handleError(err)
	doRequest(req, client, err)

	url = "http://localhost:4080/quote/random"
	log.Println("URL:>", url)
	jsonStr = []byte(``)
	req, err = http.NewRequest("GET", url, bytes.NewBuffer(jsonStr))
	handleError(err)
	doRequest(req, client, err)
}

// doRequest making a client request
func doRequest(req *http.Request, client *http.Client, err error) {
	if err != nil {
		log.Fatal(err)
	}
	resp, err := client.Do(req)
	handleError(err)
	defer resp.Body.Close()

	log.Println("response Status:", resp.Status)
	body, _ := io.ReadAll(resp.Body)
	log.Println("response Body:", string(body))
	fmt.Println()
}

// handleError is handling with errors
func handleError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
