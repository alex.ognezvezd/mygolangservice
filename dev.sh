#!/bin/bash
NETWORK_NAME=meow_network
if [ -z $(docker network ls --filter name=^${NETWORK_NAME}$ --format="{{ .Name }}") ] ; then 
    docker network create ${NETWORK_NAME} ; 
fi
cp .env.dev .env

docker compose up -d db cache

go run ./cmd/api/main.go 

docker compose down
