## My golang service

My golang service allows user to make quote service.

## Getting started

Change .env.prod with your database and server parameters. Also you can change authentication parameters.
Run `prod.sh` to automatically run service
Path to client: `cmd/apiclient/main.go`

## Message format

You will get log messages
