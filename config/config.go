// Package conf implements access to the project config variables and their usage
package conf

import (
	"github.com/ilyakaznacheev/cleanenv"
	"github.com/joho/godotenv"
	"gitlab.com/golight/cache"
	"gitlab.com/golight/dao/params"
)

// AppConfig конфигурация приложения
type AppConfig struct {
	Name       string    `env:"APP_NAME"`   // name of the project
	Production bool      `env:"PRODUCTION"` // is it production stage
	Db         params.DB // database reference
	Cache      cache.Config // cache config parameters
}

// NewAppConfig конструктор конфигурации приложения
func NewAppConfig(env ...string) (*AppConfig, error) {
	var err error
	conf := &AppConfig{}
	err = godotenv.Load(env...)
	if err != nil {
		return nil, err
	}

	err = cleanenv.ReadEnv(conf)
	if err != nil {
		return nil, err
	}

	return conf, nil
}
