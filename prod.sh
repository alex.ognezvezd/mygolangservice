#!/bin/bash
NETWORK_NAME=meow_network
if [ -z $(docker network ls --filter name=^${NETWORK_NAME}$ --format="{{ .Name }}") ] ; then 
    docker network create ${NETWORK_NAME} ; 
fi
cp .env.prod .env
docker compose down --remove-orphans
docker compose up --force-recreate --build -d
go run ./cmd/api/main.go 
